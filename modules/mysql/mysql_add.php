<?php

include_once('modules/mysql/mysql_conf.php');
$conn = new mysqli($IP, $USER, $PASS, $DB, $PORT);

 $add = isset($_POST['add']) ? $_POST['add'] : '';
 $name = isset($_POST['name']) ? $_POST['name'] : '';
 $rom = isset($_POST['rom']) ? $_POST['rom'] : '';
 $min = isset($_POST['min']) ? $_POST['min'] : '';
 $max = isset($_POST['max']) ? $_POST['max'] : '';
 $action = isset($_POST['action']) ? $_POST['action'] : '';
 $new_name = isset($_POST['new_name']) ? $_POST['new_name'] : '';
  
 
 if (($add == "add")){
 	if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
	} 

	$sql = "CREATE TABLE `".$name."_".$rom."` (
	id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	time TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	value FLOAT
	)";

	if ($conn->query($sql) === TRUE) {
    	echo "Table $name created successfully";
	} else {
   	 echo "Error creating table: " . $conn->error;
	}		

	$sqlt = "CREATE TABLE `thresholds` (
	id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	rom_name varchar(255) UNIQUE,
	name varchar(255) UNIQUE,
	rom varchar(255) UNIQUE,
	min varchar(255),
	max varchar(255)
	)";

	if ($conn->query($sqlt) === TRUE) {
    	echo "Table $name created successfully";
	} else {
   	 echo "Error creating table: " . $conn->error;
	}		
	
	$sqla = "INSERT INTO thresholds (rom_name, name, rom, min, max) VALUES ('".$name."_".$rom."','$name','$rom','$min','$max')";


	if ($conn->query($sqla) === TRUE) {
    	echo "Table $name created successfully";
	} else {
   	 echo "Error creating table: " . $conn->error;
	}
	
	
	
	
	
	$conn->close();
       
    header("location: " . $_SERVER['REQUEST_URI']);
    exit();
}
	
	

 if (($action== "rm")){
	if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
	} 

	$sql = "DROP TABLE `".$name."_".$rom."`";

	if ($conn->query($sql) === TRUE) {
    	echo "Table $rom removed successfully";
	} else {
   	 echo "Error removing table: " . $conn->error;
	}		
	
	$sqld = "DELETE FROM thresholds WHERE rom='$rom'";

	if ($conn->query($sqld) === TRUE) {
    	echo "Table $rom removed successfully";
	} else {
   	 echo "Error removing table: " . $conn->error;
	}		
	$conn->close();
     
   header("location: " . $_SERVER['REQUEST_URI']);
   exit();
    	
}


 if (($action== "new")){
	if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
	} 

	$sql = "RENAME TABLE `".$new_name."` TO `".$name."_".$rom."`";

	if ($conn->query($sql) === TRUE) {
    	echo "Table $rom removed successfully";
	} else {
   	 echo "Error removing table: " . $conn->error;
	}		
	$conn->close();
     
    header("location: " . $_SERVER['REQUEST_URI']);
    exit();
    	
}
    
  

?> 

<div class="panel panel-default">
<div class="panel-heading">Sensors</div>

<div class="table-responsive">
<table class="table table-hover table-condensed small" border="0">

<?php


$db = new PDO('sqlite:dbf/nettemp.db');
$rows = $db->query("SELECT * FROM sensors ORDER BY position ASC");
$row = $rows->fetchAll();

   foreach ($row as $a) { 	
   	$temp[]=$a['name']."_".$a['rom'];
   }


    $res = $conn->query("SHOW TABLES");
	 while ($rowd = mysqli_fetch_array($res)) {
    if (!in_array($rowd[0], $temp) && ($rowd[0]!='thresholds')) {
		$changed[]=$rowd[0];
    }
	}    




?>
<thead>
<tr>
<th>Rom</th>
<th>Remote SQL server</th>
<th>Rename old table</th>
<th>Create table</th>
<th>Delete table</th>
</tr>
</thead>



<?php 
    foreach ($row as $a) { 	
?>
<tr>
	
		
    <td class="col-md-1">
		<?php echo $a['name']."_".$a['rom']; ?>
	</td>
	<td class="col-md-1">
	<?php
	$rom=$a['name']."_".$a['rom'];
	$check_exists = $conn->query("SHOW TABLES LIKE '$rom'");
	$table_exists = $check_exists->num_rows;
	if ($table_exists > 0) { 
	?>
	<span class="label label-success">Table exist</span>
<?php
	} 
	else { 
	?>
	<span class="label label-danger">Table not exist</span>
<?php
	}	


	
	?>
	</td>
	<td class="col-md-1">
   	<form action="" method="post" style="display:inline!important;">
   	<select name="new_name">
   	<?php
   		 foreach ($changed as $new) { 
  		?>
			<option value="<?php echo $new; ?>"><?php echo $new; ?></option>
		<?php 
			} 
		?>
   	</select>
		<input type="hidden" name="rom" value="<?php echo $a['rom']; ?>" />
		<input type="hidden" name="name" value="<?php echo $a['name']; ?>" />
		<input type="hidden" name="action" value="new" />
		<button class="btn btn-xs btn-info"><span class="glyphicon glyphicon-pencil"></span> </button>
    </form>
    </td>

    <td class="col-md-1">
    <form action="" method="post" style="display:inline!important;"> 	
	<input type="hidden" name="rom" value="<?php echo $a['rom']; ?>" />
	<input type="hidden" name="name" value="<?php echo $a['name']; ?>" />
	<input type="hidden" name="min" value="<?php echo $a['tmp_min']; ?>" />
	<input type="hidden" name="max" value="<?php echo $a['tmp_max']; ?>" />
	<button class="btn btn-xs btn-success"><span class="glyphicon glyphicon-plus"></span> </button>
	<input type="hidden" name="add" value="add" />
    </form>
  
	<td class="col-md-1">
    <form action="" method="post" style="display:inline!important;">
    	<input type="hidden" name="name" value="<?php echo $a['name']; ?>" />
		<input type="hidden" name="rom" value="<?php echo $a['rom']; ?>" />
		<input type="hidden" name="action" value="rm" />
		<button class="btn btn-xs btn-danger"><span class="glyphicon glyphicon-trash"></span> </button>
    </form>
    </td>


</tr>

<?php 

}  

?>
</table>
</div>
</div>
